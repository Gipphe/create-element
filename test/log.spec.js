var chai = require('chai');
chai.should();
var sinon = require('sinon');

var log = require('../lib/log');

describe('log', function() {
	beforeEach(function() {
		this.log = sinon.spy(console, 'log');
	});
	afterEach(function() {
		this.log.restore();
	});
	it('should log deprecation warning', function() {
		log('foo');
		this.log.calledOnce.should.equal(true);
		this.log.calledWith('foo is deprecated and should not be used').should.equal(true);
	});
});