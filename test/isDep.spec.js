var chai = require('chai');
chai.should();

var isDep = require('../lib/isDep');

describe('isDep', function() {
	it('should return true for expected elements', function() {
		var deps = [
			'acronym',
			'applet',
			'basefont',
			'big',
			'center',
			'dir',
			'font',
			'frame',
			'frameset',
			'noframes',
			'strike',
			'tt'
		];
		deps.forEach(function(dep) {
			isDep(dep).should.equal(true);
		});
		isDep('foo').should.not.equal(true);
	});
});