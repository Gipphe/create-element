var chai = require('chai');
chai.should();

var sinon = require('sinon');
var pq = require('proxyquire');

var jquery = sinon.stub();
var isDep = sinon.stub().returns(false);
var log = sinon.stub();
var mocks = {
	jquery: jquery,
	'./isDep': isDep,
	'./log': log
};

var makeEl = pq('../lib/makeEl', mocks);

describe('makeEl', function() {
	beforeEach(function() {
		jquery.reset();
		isDep.reset();
		log.reset();
	});
	it('should call jquery with the element', function() {
		makeEl();
		jquery.calledOnce.should.equal(true);
	});
	it('should pass element string to jquery', function() {
		makeEl('tr');
		jquery.calledWith('<tr></tr>').should.equal(true);
	});
	it('should call log when isDep returns true', function() {
		isDep.returns(true);
		makeEl('tr');
		jquery.calledOnce.should.equal(true);
		isDep.calledOnce.should.equal(true);
		log.calledOnce.should.equal(true);
		log.calledWith('tr').should.equal(true);
	});
});