var chai = require('chai');
chai.should();
var sinon = require('sinon');

var pq = require('proxyquire');
var makeElMock = sinon.stub();
var ce = pq('../index.js', { './lib/makeEl': makeElMock });

describe('create-element', function() {
	it('should return a function with multiple properties', function() {
		ce.should.be.a('function');
		ce.should.contain.all.keys('td', 'tr', 'a');
		ce.td.should.be.a('function');
		ce.tr.should.be.a('function');
		ce.a.should.be.a('function');
	});
});