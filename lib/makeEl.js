var $ = require('jquery');
var isDep = require('./isDep');
var log = require('./log');
module.exports = function(el) {
	if (isDep(el)) {
		log(el);
	}
	return $('<' + el + '></' + el + '>');
};
