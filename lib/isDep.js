var deps = require('./deprecated');
module.exports = function(el) {
	var isDep = false;
	deps.forEach(function(dep) {
		if (dep === el.toLowerCase()) {
			isDep = true;
		}
	});
	return isDep;
};
